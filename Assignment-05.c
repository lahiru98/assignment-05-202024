#include <stdio.h>
#define PerformanceCost 500
#define PerParticipant 3

    int Participants(int price)
    {
    return (120-(price-15)/5*20);
    }

    int revenue(int price)
    {
    return price*Participants(price);
    }

    int cost(int price){
    return PerformanceCost + Participants(price)*PerParticipant;
    }

    int profit(int price)
    {
    return revenue(price)-cost(price);
    }

    int main()
{
    printf("Expected profit for Ticket price (Rs.5 to Rs.50)\n\n");

    printf("Relationship between the ticket price and the profit\n");
    printf("Ticket Price \t Profit\n");

    int ticketPrice;
	for(ticketPrice = 5 ; ticketPrice<=50; ticketPrice+=5)
        {
        printf("Rs.%d \t\t Rs.%d\n", ticketPrice, profit(ticketPrice));
        }
    printf("\nThe highest profit is at the ticket price of Rs.25\n\n");
    return 0;
}
